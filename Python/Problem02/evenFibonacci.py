# evenFibonacci.py
# 4000000

i1 = 0
i2 = 1
sum = 0

while i2 < 4000000:
    if i2 % 2 == 0:
        sum += i2
    tmp = i1 + i2
    i1 = i2
    i2 = tmp

print(sum)
