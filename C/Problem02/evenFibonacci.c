/* evenFibonacci.c */
#include <stdio.h>

int main(void) {
  int i1 = 0;
  int i2 = 1;
  int sum = 0;
  int tmp;

  while (i2 < 4000000) {
    if (i2 % 2 == 0) {
      sum += i2;
    }
    tmp = i1 + i2;
    i1 = i2;
    i2 = tmp;
  }
  printf("Summe: %d\n", sum);
  return 0;
}
